import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

/*Angular Material*/
// import { MatToolbarModule } from '@angular/material/toolbar';
// import {MatIconModule} from '@angular/material/icon';
// import {MatButtonModule} from '@angular/material/button';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './Layout/login/login.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { GridDataComponent } from './Shared/grid-data/grid-data.component';
import { HeaderComponent } from './Layout/header/header.component';
// import { MainComponent } from './Layout/main/main.component';

// import { CustomMaterialModule } from './Module/custom-material.module';

@NgModule({
  declarations: [
    AppComponent,
    // MainComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    // CustomMaterialModule
    /*MatToolbarModule,
    MatIconModule,
    MatButtonModule*/
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
