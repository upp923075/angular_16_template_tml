import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { DashboardComponent } from '../app/Components/dashboard/dashboard.component'

const routes: Routes = [
  // {path:"",component:DashboardComponent},
  {path:'',loadChildren: () => import('../app/Module/root-module/root-module.module').then(m => m.RootModuleModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
