import { Component, ViewChild, AfterViewInit } from '@angular/core';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';

import UsersJson from './users.json';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: string;
  symbol: string;
}


@Component({
  selector: 'app-grid-data',
  templateUrl: './grid-data.component.html',
  styleUrls: ['./grid-data.component.css']
})
export class GridDataComponent {

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];

  dataSource = new MatTableDataSource<PeriodicElement>(UsersJson);
  @ViewChild(MatPaginator) paginator :any = MatPaginator;

  constructor(){
    console.log('hi');
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

}
