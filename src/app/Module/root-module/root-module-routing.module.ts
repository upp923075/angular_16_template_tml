import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from 'src/app/Layout/login/login.component';
import { DashboardComponent } from 'src/app/Components/dashboard/dashboard.component';
import { MainComponent } from 'src/app/Layout/main/main.component';

const routes: Routes = [
  { path:"",component:LoginComponent },
  { path : "app", component: MainComponent, 
  children:[
    {path:'dashboard',component:DashboardComponent},
  ]}
  ];

// @NgModule({
//   declarations: [],
//   imports: [
//     CommonModule
//   ]
// })

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RootModuleRoutingModule { }
