import { CUSTOM_ELEMENTS_SCHEMA,NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RootModuleRoutingModule } from './root-module-routing.module';
import { CustomMaterialModule } from '../custom-material.module';
import { HeaderComponent } from '../../Layout/header/header.component';
import { LoginComponent } from 'src/app/Layout/login/login.component'; 
import { DashboardComponent } from 'src/app/Components/dashboard/dashboard.component'; 
import { GridDataComponent } from 'src/app/Shared/grid-data/grid-data.component';
import { MainComponent } from 'src/app/Layout/main/main.component';

@NgModule({
  declarations: [
    HeaderComponent,
    LoginComponent,
    DashboardComponent,
    GridDataComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    RootModuleRoutingModule,
    CustomMaterialModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RootModuleModule { }
